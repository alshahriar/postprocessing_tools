function adjA = adjointQ(A, Q)

adjA = Q\A'*Q;

end